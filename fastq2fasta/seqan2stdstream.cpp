#include <seqan/seq_io.h>

using namespace seqan;

int main(int, char ** argv)
{
    CharString id;
    Dna5String seq;
    CharString qual;

    std::ifstream is{argv[1]};
    std::ofstream os{argv[2]};

auto it = seqan::Iter<std::ifstream, seqan::StreamIterator<seqan::Input> >(is);

    while (!atEnd(it))
    {
        readRecord(id, seq, qual, it, Fastq{});
        writeRecord(os, id, seq, qual, Fasta{});
    }

    return 0;
}
