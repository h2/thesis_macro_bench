#include <seqan/seq_io.h>

using namespace seqan;

int main(int, char ** argv)
{
    CharString id;
    Dna5String seq;
    CharString qual;

    SeqFileIn seqFileIn(argv[1]);
    SeqFileOut seqFileOut(argv[2]);

    while (!atEnd(seqFileIn))
    {
        readRecord(id, seq, qual, seqFileIn);
        writeRecord(seqFileOut, id, seq, qual);
    }

    return 0;
}
