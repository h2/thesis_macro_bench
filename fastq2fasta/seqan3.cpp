#include <iostream>

#include <seqan3/core/detail/type_inspection.hpp>
#include <seqan3/io/sequence_file/all.hpp>
#include <seqan3/range/views/async_input_buffer.hpp>
#include <seqan3/range/views/persist.hpp>

using namespace seqan3;

int main(int, char ** argv)
{
    sequence_file_input{argv[1]} | sequence_file_output{argv[2]};
}
