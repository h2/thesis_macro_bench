import sys
from Bio import SeqIO
import gzip

def reader(filename):
    f = open(filename, 'rb')
    if f.read(2) == b'\x1f\x8b':
        f.seek(0)
        return gzip.open(filename, 'rt')
    else:
        f.seek(0)
        g = open(filename, 'r')
        return g


def writer(filename):
    if filename[-3:] == '.gz':
        f = open(filename, 'wb')
        return gzip.open(filename, 'wt')
    else:
        f = open(filename, 'w')
        return f

with writer(sys.argv[2]) as output:
    for record in SeqIO.parse(reader(sys.argv[1]), "fastq"):
        output.write(record.format("fasta"))


