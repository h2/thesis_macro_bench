# FastQ to FastA conversion

Unzip `ERR242939_1.filt.fastq` and try with zipped/unzipped input/output each.

Run `/usr/bin/time -l BINARY input.fastq[.gz] output.fast[.gz]` to measure runtime (on FreeBSD or macOS).
For BioPython benchmarks replace BINARY with `python-3.6 source.py`.


# Build instructions

Possibly add `-march=native` to any of the following.

## LibGenomeTools

`gcc7 -Wall -Wextra -pedantic -O3 -I /path/to/genometools/src gt.c /path/to/genometools/lib/libgenometools.a -lm -o gt -DNDEBUG`

## SeqAn2

`g++7 -std=c++17 -fconcepts  -I ~/devel/seqan/include seqan2.cpp -Wall -Wextra -pedantic -lexecinfo -lstdc++fs -O3 -DNDEBUG -o seqan2 -lz -DSEQAN_HAS_ZLIB=1 -pthread`

Exchange `seqan2.cpp` with `seqan2stdstream.cpp` for the std::ios-based benchmark.

## SeqAn3

`g++7 -std=c++17 -fconcepts -I /path/to/seqan3/submodules/range-v3/include -I /path/to/seqan3/submodules/sdsl-lite/include -I /path/to/seqan3/include seqan3.cpp -Wall -Wextra -pedantic -lexecinfo -lstdc++fs -O3 -DNDEBUG -o seqan3 -lz -DSEQAN3_HAS_ZLIB=1 -pthread`

Possibly add `-DSEQAN3_WORKAROUND_VIEW_PERFORMANCE=0`.





