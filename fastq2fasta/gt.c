#include <string.h>
#include <core/fa_api.h>
#include <core/fasta.h>
#include <core/ma_api.h>
#include <core/seq_iterator_api.h>
#include <core/seq_iterator_fastq_api.h>
#include <core/seq_iterator_sequence_buffer_api.h>

int main(int argc, char **argv)
{
    /* setup */
    gt_ma_init(false);
    gt_fa_init();

    GtError *err = gt_error_new();
    GtStrArray *filenames = gt_str_array_new();
    gt_str_array_add_cstr(filenames, argv[1]);

    GtSeqIterator* seqit = gt_seq_iterator_sequence_buffer_new(filenames, err);
    const GtUchar *sequence = NULL;
    GtUword len = 0;
    char *desc = NULL;

    /* output stuffs*/
    GtError *err2 = gt_error_new();
    GtFile *fout = gt_file_new(argv[2], "w", err2);

    while (gt_seq_iterator_next(seqit, &sequence,
                            &len, &desc, err) == 1)
    {
        gt_fasta_show_entry(desc, (char*)sequence, len, 70, fout);
    }

    /* cleanup */
    gt_seq_iterator_delete(seqit);
    gt_str_array_delete(filenames);
    gt_error_delete(err);
    gt_error_delete(err2);
    gt_file_delete(fout);
}
