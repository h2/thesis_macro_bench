/* BASED ON CODE BY CHRISTOPHER POCKRANDT */

#include <sdsl/vectors.hpp>
#include <sdsl/suffix_arrays.hpp>

#include <seqan/index.h>
#include <seqan/seq_io.h>

#include <seqan3/alphabet/nucleotide/dna4.hpp>
#include <seqan3/io/sequence_file/format_fasta.hpp>
#include <seqan3/io/sequence_file/input.hpp>
#include <seqan3/search/all.hpp>

// #define MYDEBUG 1

constexpr unsigned iterations = 100;
constexpr unsigned sa_samling_rate = 10;
constexpr unsigned isa_samling_rate = 10000000;

// simulate reads using mason
// ./mason illumina -n 200 -N 1000000 --no-N -pi 0 -pd 0 -pmm 0 -pmmb 0 -pmme 0 -hs 0 -hi 0 -hm 0 -hM 0 --haplotype-no-N -spA 0 -spC 0 -spG 0 -f /srv/public/cpockrandt/epr2-data/chr13.fa --read-name-prefix "" -o /srv/public/cpockrandt/epr2-data/reads.fa

namespace seqan
{
    template <typename TChar, typename TOwner>
    struct SAValue<String<TChar, TOwner> >
    {
        typedef uint32_t Type;
    };
};

std::string mytime()
{
    using namespace std;
    auto r = time(nullptr);
    auto c = ctime(&r);
    std::string buf(c);
    buf.insert(0, "[");
    buf.append("] ");
    buf.erase(remove(buf.begin(), buf.end(), '\n'), buf.end());
    return buf;
}

using csa_wt_t = sdsl::csa_wt<sdsl::wt_blcd<sdsl::bit_vector,
                                            sdsl::rank_support_v<>,
                                            sdsl::select_support_scan<>,
                                            sdsl::select_support_scan<0>>,
                              sa_samling_rate,
                              isa_samling_rate,
                              sdsl::sa_order_sa_sampling<>,
                              sdsl::isa_sampling<>,
                              sdsl::plain_byte_alphabet>;

using csa_epr_t = sdsl::csa_wt<sdsl::epr::wt_epr<5>,
                               sa_samling_rate,
                               isa_samling_rate,
                               sdsl::sa_order_sa_sampling<>,
                               sdsl::isa_sampling<>,
                               sdsl::byte_alphabet>;

template <typename wt_t>
void benchSDSL(std::string const & path, std::vector<sdsl::int_vector<8> > const & sdsl_reads)
{
    using namespace sdsl;
    wt_t index_fwd, index_rev;

    load_from_file(index_fwd, path + ".fwd");
    load_from_file(index_rev, path + ".rev");

    const unsigned read_len = sdsl_reads[0].size();

    uint64_t l0, l1, r0, r1;

    std::clock_t c_start = std::clock();
    auto t_start = std::chrono::high_resolution_clock::now();

    unsigned hits = 0;
    for (unsigned i = 0; i < iterations; ++i)
    {
        for (unsigned r = 0; r < sdsl_reads.size(); ++r)
        {
            l0 = 0;
            l1 = 0;
            r0 = index_fwd.size() - 1;
            r1 = index_rev.size() - 1;
            for (unsigned l = read_len/2; l < read_len; ++l) {
                bidirectional_search(index_rev, l0, r0, l1, r1, sdsl_reads[r][l], l0, r0, l1, r1);
            }
            for (signed l = read_len/2-1; l >= 0; --l)
                bidirectional_search(index_fwd, l1, r1, l0, r0, sdsl_reads[r][l], l1, r1, l0, r0);
            hits += r0 - l0 + 1;
            #ifdef MYDEBUG
                if (r0 - l0 + 1 != 1 || r1 - l1 + 1 != 1)
                    std::cout << "Occ(" << r << "): " << (r0 - l0 + 1) << " - " << (r1 - l1 + 1) << "\n";
            #endif
        }

    }

    std::clock_t c_end = std::clock();
    auto t_end = std::chrono::high_resolution_clock::now();

    std::cout << std::fixed << std::setprecision(2) << "       CPU time: "
              << (1000.0 * (c_end-c_start) / CLOCKS_PER_SEC / iterations) << " ms\n"
              << "Wall clock time: "
              << std::chrono::duration<double, std::milli>(t_end-t_start).count()
              << " ms (Hits: " << hits << ")\n";
}

template <typename TIndexConfig>
void benchSeqAn(seqan::CharString const & path, seqan::StringSet<seqan::String<seqan::Dna>> const & seqan_reads)
{
    using namespace seqan;
    typedef Index<String<seqan::Dna>, TIndexConfig> TIndex;
    TIndex index;
    open(index, toCString(path));
    typedef Iter<TIndex, VSTree<TopDown<> > > TIter;
    TIter it(index);

    const unsigned read_len = length(seqan_reads[0]);

    std::clock_t c_start = std::clock();
    auto t_start = std::chrono::high_resolution_clock::now();
    unsigned hits = 0;

    for (unsigned i = 0; i < iterations; ++i)
    {
        for (unsigned r = 0; r < length(seqan_reads); ++r)
        {
            for (unsigned l = read_len/2; l < read_len; ++l)
                goDown(it, seqan_reads[r][l], Rev());
            for (signed l = read_len/2-1; l >= 0; --l)
                goDown(it, seqan_reads[r][l], Fwd());
            hits += countOccurrences(it.fwdIter);
            #ifdef MYDEBUG
                if (countOccurrences(it.fwdIter) != 1 || countOccurrences(it.revIter) != 1)
                    std::cout << "Occ(" << r << "): " << countOccurrences(it.fwdIter) << " - " << countOccurrences(it.revIter) << "\n";
            #endif
            goRoot(it);
        }
    }

    std::clock_t c_end = std::clock();
    auto t_end = std::chrono::high_resolution_clock::now();

    std::cout << std::fixed << std::setprecision(2) << "       CPU time: "
              << (1000.0 * (c_end-c_start) / CLOCKS_PER_SEC / iterations) << " ms\n"
              << "Wall clock time: "
              << std::chrono::duration<double, std::milli>(t_end-t_start).count()
              << " ms (Hits: " << hits << ")\n";
}


template <typename TIndexConfig>
void benchSeqAn3(std::string const & path, std::vector<std::vector<seqan3::dna4>> const & seqan_reads)
{
    seqan3::bi_fm_index<seqan3::dna4, seqan3::text_layout::single, TIndexConfig> index;

    {
        std::ifstream is{path, std::ios::binary};
        cereal::BinaryInputArchive archive{is};
        archive(index);
    }

    auto it = index.begin();

    const unsigned read_len = std::ranges::size(seqan_reads[0]);

    std::clock_t c_start = std::clock();
    auto t_start = std::chrono::high_resolution_clock::now();

    unsigned hits = 0;

    for (unsigned i = 0; i < iterations; ++i)
    {

        for (unsigned r = 0; r < std::ranges::size(seqan_reads); ++r)
        {
            it.extend_right(seqan_reads[r] | seqan3::views::drop(read_len/2));
            it.extend_left(seqan_reads[r] | seqan3::views::take(read_len/2));
            hits += it.count();

            it = index.begin();
        }

    }

    std::clock_t c_end = std::clock();
    auto t_end = std::chrono::high_resolution_clock::now();

    std::cout << std::fixed << std::setprecision(2) << "       CPU time: "
              << (1000.0 * (c_end-c_start) / CLOCKS_PER_SEC / iterations) << " ms\n"
              << "Wall clock time: "
              << std::chrono::duration<double, std::milli>(t_end-t_start).count()
              << " ms (Hits: " << hits << ")\n";
}

int main(int argc, char** argv)
{
    if (argc != 2)
        return 42;

    std::string dir = argv[1];

    std::string reads_path = dir + "/reads.fa";
    std::string seqan_wt_path = dir + "/seqan-wt/index";
    std::string seqan_epr_path = dir + "/seqan-epr/index";
    std::string seqan3_wt_path = dir + "/seqan3-wt/index";
    std::string seqan3_epr_path = dir + "/seqan3-epr/index";
    std::string sdsl_wt_false_path = dir + "/sdsl-wt-false/index";
    std::string sdsl_wt_true_path = dir + "/sdsl-wt-true/index";
    std::string sdsl_epr_true_path = dir + "/sdsl-epr-true/index";
    std::string sdsl_epr_false_path = dir + "/sdsl-epr-false/index";

    seqan::StringSet<seqan::String<seqan::Dna>> seqan_reads;
    std::vector<std::vector<seqan3::dna4>> seqan3_reads;
    std::vector<sdsl::int_vector<8> > sdsl_reads;

    // Read reads
    {
        using namespace seqan;
        StringSet<CharString> ids;
        SeqFileIn seqFileIn(toCString(reads_path));
        readRecords(ids, seqan_reads, seqFileIn);

        // Copy reads to int_vectors
        sdsl_reads.resize(length(seqan_reads));
        for (unsigned i = 0; i < length(seqan_reads); ++i)
        {
            sdsl_reads[i].resize(length(seqan_reads[i]));
            for (unsigned j = 0; j < length(seqan_reads[i]); ++j)
                sdsl_reads[i][j] = ordValue(seqan_reads[i][j]) + 1;
        }
    }

    {
        struct my_traits : seqan3::sequence_file_input_default_traits_dna
        {
            using sequence_alphabet = seqan3::dna4;
        };

        seqan3::sequence_file_input<my_traits, seqan3::fields<seqan3::field::seq>> f{reads_path};

        for (auto & [ seq ] : f)
            seqan3_reads.push_back(seq);
    }

    std::cout << "--------------------- Start searching.\n";

#if 1
    // SeqAn WT
    {
        using namespace seqan;
        typedef FMIndexConfig<void, uint32_t, 2, 1> TMyConfig;
        typedef BidirectionalIndex<FMIndex<void, TMyConfig> > TIndexConfig;
        benchSeqAn<TIndexConfig>(seqan_wt_path, seqan_reads);
        std::cout << "--------------------- SeqAn WT finished.\n";
    }
#endif
#if 1
    // SeqAn3 WT
    {
        benchSeqAn3<csa_wt_t>(seqan3_wt_path, seqan3_reads);
        std::cout << "--------------------- SeqAn3 WT finished.\n";
    }
#endif
#if 1
    // SDSL-WT<false>
    {
        benchSDSL<csa_wt_t>(sdsl_wt_false_path, sdsl_reads);
        std::cout << "--------------------- SDSL-WT<false> finished.\n";
    }
#endif

#if 1
    // SeqAn 2-level EPR dict
    {
        using namespace seqan;
        typedef FastFMIndexConfig<void, uint32_t, 2, 1> TMyFastConfig;
        typedef BidirectionalIndex<FMIndex<void, TMyFastConfig> > TIndexConfig;
        benchSeqAn<TIndexConfig>(seqan_epr_path, seqan_reads);
        std::cout << "--------------------- SeqAn 2-level EPR dict finished.\n";
    }
#endif
#if 1
    // SeqAn3 EPR
    {
        benchSeqAn3<csa_epr_t>(seqan3_epr_path, seqan3_reads);
        std::cout << "--------------------- SeqAn3 EPR finished.\n";
    }
#endif
#if 1
    // SDSL-EPR<false> v
    {
        benchSDSL<csa_epr_t>(sdsl_epr_false_path, sdsl_reads);
        std::cout << "--------------------- SDSL-EPR<true> v finished.\n";
    }
#endif

}
