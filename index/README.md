# Index benchmark

Requires the following branches:

  * SeqAn3 from https://github.com/h-2/seqan3 at `54e1712ef7fec4c33f41d3775b6fb066a3a47f9e`.
  * SDSL-submodule from https://github.com/eseiler/sdsl-lite at `b9ea70b9f7c77551340927a4bb3335e3aefe846d`.


Adjust the paths in `CMakeLists.txt` and build out-of-source in e.g. `~/devel/index_bench` with:

```
% cmake /path/to/src -DCMAKE_CXX_COMPILER=g++7 -DCMAKE_BUILD_TYPE=Release [-DCMAKE_CXX_FLAGS="-march=native"]
% [g]make -j 2
```

Create a sequence directory, e.g. `~/sequences/index_bench/` and move `chr13.fasta` (unzip first) and `reads.fa` there.
Create the following sub-directories: `sdsl-epr-false sdsl-wt-false seqan-epr seqan-wt seqan3-epr seqan3-wt`

Go back into `~/devel/index_bench` and create the indexes:
```
% ./build ~/sequences/index_bench/
```

Run the benchmark:

```
% ./bench ~/sequences/index_bench/
```




