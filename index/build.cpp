/* BASED ON CODE BY CHRISTOPHER POCKRANDT */

#include <sdsl/suffix_arrays.hpp>

#include <seqan/index.h>
#include <seqan/seq_io.h>

#include <seqan3/alphabet/nucleotide/dna4.hpp>
#include <seqan3/search/all.hpp>

constexpr unsigned sa_samling_rate = 10;
constexpr unsigned isa_samling_rate = 10000000;

namespace seqan
{
    template <typename TChar, typename TOwner>
    struct SAValue<String<TChar, TOwner> >
    {
        typedef uint32_t Type;
    };
};

std::string mytime()
{
    auto r = time(nullptr);
    auto c = ctime(&r);
    std::string buf(c);
    buf.insert(0, "[");
    buf.append("] ");
    buf.erase(remove(buf.begin(), buf.end(), '\n'), buf.end());
    return buf;
}

using csa_wt_t = sdsl::csa_wt<sdsl::wt_blcd<sdsl::bit_vector,
                                            sdsl::rank_support_v<>,
                                            sdsl::select_support_scan<>,
                                            sdsl::select_support_scan<0>>,
                              sa_samling_rate,
                              isa_samling_rate,
                              sdsl::sa_order_sa_sampling<>,
                              sdsl::isa_sampling<>,
                              sdsl::plain_byte_alphabet>;

using csa_epr_t = sdsl::csa_wt<sdsl::epr::wt_epr<5>,
                               sa_samling_rate,
                               isa_samling_rate,
                               sdsl::sa_order_sa_sampling<>,
                               sdsl::isa_sampling<>,
                               sdsl::byte_alphabet>;


int main(int argc, char** argv)
{
    if (argc != 2)
        return 42;

    std::string dir = argv[1];

    std::string genome_path = dir + "/chr13.fa";
    std::string seqan_wt_path = dir + "/seqan-wt/index";
    std::string seqan_epr_path = dir + "/seqan-epr/index";
    std::string seqan3_wt_path = dir + "/seqan3-wt/index";
    std::string seqan3_epr_path = dir + "/seqan3-epr/index";
    std::string sdsl_wt_false_path = dir + "/sdsl-wt-false/index";
    std::string sdsl_wt_true_path = dir + "/sdsl-wt-true/index";
    std::string sdsl_epr_true_v_path = dir + "/sdsl-epr-true/index";
    std::string sdsl_epr_false_path = dir + "/sdsl-epr-false/index";

    seqan::String<seqan::Dna> seqan_genome;
    sdsl::int_vector<8> sdsl_genome, sdsl_genome_rev;
    std::vector<seqan3::dna4> seqan3_genome;

    // Read fasta input file
    {
        using namespace seqan;
        CharString id;
        SeqFileIn seqFileIn(toCString(genome_path));
        readRecord(id, seqan_genome, seqFileIn);

        // Copy text to int_vector
        sdsl_genome.resize(length(seqan_genome));
        sdsl_genome_rev.resize(length(seqan_genome));
        seqan3_genome.resize(length(seqan_genome));
        for (unsigned i = 0; i < length(seqan_genome); ++i)
        {
            sdsl_genome[i] = ordValue(seqan_genome[i]) + 1;
            sdsl_genome_rev[length(seqan_genome) - i - 1] = ordValue(seqan_genome[i]) + 1;

            seqan3::assign_char_to((char)seqan_genome[i], seqan3_genome[i]);
        }
    }

    std::cout << "Check:\n";
    std::cout << "   SeqAn: " << seqan::prefix(seqan_genome, 100) << "\n";
    std::cout << "   SDSL : ";
    for (unsigned i = 0; i < std::min((uint64_t)100u, (uint64_t)sdsl_genome.size()); ++i)
        std::cout << (unsigned)sdsl_genome[i];
    std::cout << "\n";
    std::cout << mytime() << "Start building indices.\n";

#if 1
    // SDSL-WT<false>
    {
        csa_wt_t index_fwd, index_rev;
        sdsl::construct_im(index_fwd, sdsl_genome, 0);
        sdsl::construct_im(index_rev, sdsl_genome_rev, 0);
        sdsl::store_to_file(index_fwd, sdsl_wt_false_path + ".fwd");
        sdsl::store_to_file(index_rev, sdsl_wt_false_path + ".rev");
        std::cout << mytime() << "SDSL-WT<false> finished.\n";
    }
#endif
#if 1
    // SDSL-EPR<false> v
    {
        csa_epr_t index_fwd, index_rev;
        sdsl::construct_im(index_fwd, sdsl_genome, 0);
        sdsl::construct_im(index_rev, sdsl_genome_rev, 0);
        sdsl::store_to_file(index_fwd, sdsl_epr_false_path + ".fwd");
        sdsl::store_to_file(index_rev, sdsl_epr_false_path + ".rev");
        std::cout << mytime() << "SDSL-EPR<false> v finished.\n";
    }
#endif
#if 1
    // SeqAn WT
    {
        using namespace seqan;
        typedef FMIndexConfig<void, uint32_t, 2, 1> TMyConfig;
        typedef BidirectionalIndex<FMIndex<void, TMyConfig> > TIndexConfig;
        typedef Index<String<seqan::Dna>, TIndexConfig> TIndex;
        TIndex index(seqan_genome);
        indexCreate(index, FibreSALF());
        save(index, toCString(seqan_wt_path));
        std::cout << mytime() << "SeqAn WT finished.\n";
    }
#endif
#if 1
    // SeqAn 2-level EPR dict
    {
        using namespace seqan;
        typedef FastFMIndexConfig<void, uint32_t, 2, 1> TMyFastConfig;
        typedef BidirectionalIndex<FMIndex<void, TMyFastConfig> > TIndexConfig;
        typedef Index<String<seqan::Dna>, TIndexConfig> TIndex;
        TIndex index(seqan_genome);
        indexCreate(index, FibreSALF());
        save(index, toCString(seqan_epr_path));
        std::cout << mytime() << "SeqAn 2-level EPR dict finished.\n";
    }
#endif
#if 1
    // SeqAn3 WT
    {
        seqan3::bi_fm_index<seqan3::dna4, seqan3::text_layout::single, csa_wt_t>
            index{seqan3_genome};
        std::ofstream os{seqan3_wt_path.c_str(), std::ios::binary};
        cereal::BinaryOutputArchive archive{os};
        archive(index);
    }
    std::cout << mytime() << "SeqAn3 WT finished.\n";
#endif
#if 1
    // SeqAn3 EPR
    {
        seqan3::bi_fm_index<seqan3::dna4, seqan3::text_layout::single, csa_epr_t>
            index{seqan3_genome};
        std::ofstream os{seqan3_epr_path.c_str(), std::ios::binary};
        cereal::BinaryOutputArchive archive{os};
        archive(index);
    }
    std::cout << mytime() << "SeqAn3 EPR finished.\n";
#endif
}
