from Bio.Seq import Seq
from Bio.Alphabet import *
import pytest
import pytest_benchmark
import time
from random import choice

def gen_random_dna(length):
    DNA=""
    for count in range(length):
        DNA+=choice("CGTA")
    return DNA

def filtr_happy(dna):
    out = Seq("", generic_dna)
    out = [ i for i in dna if 1]

def test_happy(benchmark):
    # benchmark something
    coding_dna = Seq(gen_random_dna(10000), generic_dna)
    result = benchmark(filtr_happy, coding_dna)


def filtr_rand(dna):
    out = Seq("", generic_dna)
    out = [ i for i in dna if (i == 'A' or i == 'C')]

def test_rand(benchmark):
    # benchmark something
    coding_dna = Seq(gen_random_dna(10000), generic_dna)
    result = benchmark(filtr_rand, coding_dna)


def filtr_sad(dna):
    out = Seq("", generic_dna)
    out = [ i for i in dna if 0]

def test_sad(benchmark):
    # benchmark something
    coding_dna = Seq(gen_random_dna(10000), generic_dna)
    result = benchmark(filtr_sad, coding_dna)


def baseline(dna):
    out = Seq("", generic_dna)
    for i in dna:
        out+=i
    #out = dna

def test_baseline(benchmark):
    # benchmark something
    coding_dna = Seq(gen_random_dna(10000), generic_dna)
    result = benchmark(baseline, coding_dna)
