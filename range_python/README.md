# Python micro-benchmarks

Install required packages:

```
% pip install --user biopython pytest-benchmark
```

Run micro-benchmarks:

```
% ~/.local/bin/pytest filter_bench.py
% ~/.local/bin/pytest translate_bench.py
```
