from Bio.Seq import Seq
from Bio.Alphabet import *
import pytest
import pytest_benchmark
import time
from random import choice


def gen_random_dna(length):
    DNA=""
    for count in range(length):
        DNA+=choice("CGTA")
    return DNA

def transi(dna):
    a = dna.translate()

def test_single_frame(benchmark):
    # benchmark something
    coding_dna = Seq(gen_random_dna(1000), generic_dna)
    result = benchmark(transi, coding_dna)
